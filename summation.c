///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Nicholas Tom <tom7@hawaii.edu>
// @date   01/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

char str[] = "6";

 int n = atoi(str);

printf("n = %d\n", n);

   int sum = 0;
   int i = 1;

   while(i<=n)
   {
	   sum = sum + i;
	   i++;
   }

   printf("sum = %d\n", sum);
   printf("The sum of the digits from 1 to %d is %d\n", n, sum);

   return 0;
}
